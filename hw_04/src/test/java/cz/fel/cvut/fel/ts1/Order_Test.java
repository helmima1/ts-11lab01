package cz.fel.cvut.fel.ts1;

import cz.cvut.fel.ts1.shop.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;

import java.util.ArrayList;

public class Order_Test {

    @Test
    public void constructorTest() {
        //ARRANGE
        ShoppingCart cart_test = new ShoppingCart();
        Order order_test = new Order(cart_test, "Alois", "Staropramenna");
        // act
        ArrayList<Item> cart_get_test = order_test.getItems();
        String name_test = order_test.getCustomerName();
        String adress_test = order_test.getCustomerAddress();
        //ASSERT
        Assertions.assertEquals(cart_test, cart_get_test);
        Assertions.assertEquals("Alois", name_test);
        Assertions.assertEquals("Staropramenna", adress_test);
    }

    @ParameterizedTest
    @NullSource
    public void nullConstructorTest(ShoppingCart cart_test, String name, String adress) {
        Assertions.assertEquals(null, cart_test);
        Assertions.assertEquals(null, name);
        Assertions.assertEquals(null, adress);
    }
}
