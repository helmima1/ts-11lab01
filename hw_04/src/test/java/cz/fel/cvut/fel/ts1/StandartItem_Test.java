package cz.fel.cvut.fel.ts1;

import org.junit.jupiter.api.*;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class StandartItem_Test {

    static StandardItem a;
    static StandardItem b;

    @BeforeAll
    public static void initVariable() {

        a = new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4);
    }

    // Testování StandartItem - konstruktor a copy
    @Test
    public void test_constructor() {
        //ACT
        int id_test = a.getID();
        String name_test = a.getName();
        float price_test = a.getPrice();
        String category_test = a.getCategory();
        int loyality_test = a.getLoyaltyPoints();
        StandardItem copy = a.copy();

        //ASSERT
        Assertions.assertEquals(1, id_test);
        Assertions.assertEquals("Struny_010", name_test);
        Assertions.assertEquals(150.0F, price_test);
        Assertions.assertEquals("KytaroveDoplnky", category_test);
        Assertions.assertEquals(4, loyality_test);
        Assertions.assertEquals(copy, a);
    }

    @ParameterizedTest
    @MethodSource("standardItemIndices")
    public void test_equals(StandardItem inp1, StandardItem inp2) {
        //act
        boolean eq = inp1.equals(inp2);
        //assert
        Assertions.assertEquals(true, eq);
    }

    private static Stream<Arguments> standardItemIndices() {
        return Stream.of(
                Arguments.of(new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4), new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4)),
                Arguments.of(new StandardItem(1, "Struny_011", 150.0F, "KytaroveDoplnky", 4), new StandardItem(1, "Struny_011", 150.0F, "KytaroveDoplnky", 4)),
                Arguments.of(new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 5), new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 5))
        );
    }

}
