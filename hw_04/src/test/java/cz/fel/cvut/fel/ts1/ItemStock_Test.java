package cz.fel.cvut.fel.ts1;

import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.StandardItem;
import cz.cvut.fel.ts1.storage.ItemStock;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ItemStock_Test {
    /**
     * Tato trida testuje ItemStock, ktery je inicializvany pres Storage(), jelikoz ItemStock nema public initializery
     */


    //ARRANGE
    static Item i;
    static ItemStock itemStock_test;
    static Storage storage_test;
    static int count;

    @BeforeAll
    public static void initVariable() {
        i = new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4);
        //itemStock_test = new ItemStock(i);
        count = 50;
        storage_test = new Storage();
        storage_test.insertItems(i, count);
    }

    @BeforeEach
    public void prepareVariable() throws NoItemInStorage {
        storage_test.removeItems(i, count);
        count = 50;
        storage_test.insertItems(i, count);
    }

    @ParameterizedTest
    @ValueSource(ints = {-2, 1, 2, 3, Integer.MAX_VALUE})
    public void increase_test(int incr) {
        //Act
        storage_test.insertItems(i, incr);
        count += incr;
        //ASSERT
        Assertions.assertEquals(count, storage_test.getItemCount(i));
    }


    @ParameterizedTest
    @ValueSource(ints = {-2, 0, 1, 2, 3})
    public void decrease_test(int decr) throws NoItemInStorage {
        //Act
        storage_test.insertItems(i, decr);
        storage_test.removeItems(i, decr);
        //ASSERT
        Assertions.assertEquals(count, storage_test.getItemCount(i));
    }
}
