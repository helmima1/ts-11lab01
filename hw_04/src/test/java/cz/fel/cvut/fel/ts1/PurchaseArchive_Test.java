package cz.fel.cvut.fel.ts1;

import cz.cvut.fel.ts1.archive.ItemPurchaseArchiveEntry;
import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.Item;
import cz.cvut.fel.ts1.shop.Order;
import cz.cvut.fel.ts1.shop.ShoppingCart;
import cz.cvut.fel.ts1.shop.StandardItem;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class PurchaseArchive_Test {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    static StandardItem i;
    static ShoppingCart cart_test;
    static Order order_test;
    static PurchasesArchive archive;

    @BeforeAll
    public static void initArchive() {
        // ARRANGE
        i = new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4);
        cart_test = new ShoppingCart();
        order_test = new Order(cart_test, "Alois", "Staropramenna");
        archive = new PurchasesArchive();

        cart_test.addItem(i);
        cart_test.addItem(i);
        archive.putOrderToPurchasesArchive(order_test);
    }

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    public void printItemPurchaseStatistics_putOrderToPurchasesArchive_test() {
        // Tato metoda overuje, zda byl zavolan putOrderToPurchasesArchive() tim, ze dany soubor vytiskne pres funkci printItemPurchaseStatistics(). Tim otestuji 2 funkce najednou
        // Act
        archive.printItemPurchaseStatistics();

        // ASSSERT
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:\nITEM  Item   ID 1   NAME Struny_010   CATEGORY KytaroveDoplnky   PRICE 150.0   LOYALTY POINTS 4   HAS BEEN SOLD 2 TIMES\n", outputStreamCaptor.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_test() {
        // ACT
        int res = archive.getHowManyTimesHasBeenItemSold(i);

        // ASSERT
        Assertions.assertEquals(2, res);
    }

    @Test
    public void ItemPurchaseArchiveEntry_test() {
        // Act - kontrola toho, ze se StandartItem spravne ulozil
        Item refItem = archive.getItemPurchaseArchive().get(1).getRefItem();
        // assert
        Assertions.assertEquals(i, refItem);
    }
}
