package cz.cvut.fel.ts1.main;

import cz.cvut.fel.ts1.archive.PurchasesArchive;
import cz.cvut.fel.ts1.shop.*;
import cz.cvut.fel.ts1.storage.NoItemInStorage;
import cz.cvut.fel.ts1.storage.Storage;

public class main {

    public static void main(String[] args) throws NoItemInStorage {
        /*
        Item i = new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4);
        Storage storage = new Storage();
        storage.insertItems(i, 5);
        storage.printListOfStoredItems();
        System.out.println(storage.getItemCount(1));

        storage.insertItems(i, 4);
        System.out.println(storage.getItemCount(1));
        storage.removeItems(i, 4);
        System.out.println(storage.getItemCount(1));

         */


        StandardItem a = new StandardItem(1, "Struny_010", 150.0F, "KytaroveDoplnky", 4);
        ShoppingCart cart_test = new ShoppingCart();
        cart_test.addItem(a);
        cart_test.addItem(a);
        Order order_test = new Order(cart_test, "Alois", "Staropramenna");
        PurchasesArchive archive = new PurchasesArchive();
        archive.putOrderToPurchasesArchive(order_test);
        //System.out.println(archive.getHowManyTimesHasBeenItemSold(a));
        archive.printItemPurchaseStatistics();

        EShopController eshop = new EShopController();
        //eshop.newCart(cart_test);
    }
}
