package cz.cvut.fel.ts1.selenium;

import cz.cvut.fel.ts1.selenium.config.DriverFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import java.io.IOException;

public class MapyCzTest {
    private static WebDriver driver;

    @BeforeAll
    public static void getDriver() throws IOException {
        driver = new DriverFactory().getDriver();
    }

    @Test
    public void test1() {
        driver.get("https://en.mapy.cz/zakladni?planovani-trasy");
        driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                .sendKeys("Praha"+Keys.TAB);
        driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                .sendKeys("Pardubice"+Keys.TAB);
    }

    @AfterAll
    public static void quitDriver() {
        //driver.quit();
    }
}
