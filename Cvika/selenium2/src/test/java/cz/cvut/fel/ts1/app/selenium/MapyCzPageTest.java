package cz.cvut.fel.ts1.app.selenium;

import cz.cvut.fel.ts1.app.selenium.config.DriverFactory;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class MapyCzPageTest {
    @Test
    public void fromBrnoToPrague() throws IOException {
        WebDriver driver = new DriverFactory().getDriver();
        MapyCzPage page = new MapyCzPage(driver);
        page.open();
        Route r = page.getRoute("Brno","Praha");
        System.out.println(r.getDistance());
        System.out.println(r.getTime().toString());
        driver.quit();
    }
}
