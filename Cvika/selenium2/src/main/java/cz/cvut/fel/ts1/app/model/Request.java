package cz.cvut.fel.ts1.app.model;
import lombok.*;

@Data
public class Request {
    private String question;
}
