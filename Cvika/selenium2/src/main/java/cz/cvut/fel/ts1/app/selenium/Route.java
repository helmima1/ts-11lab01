package cz.cvut.fel.ts1.app.selenium;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalTime;

@Getter
@AllArgsConstructor
public class Route {
    private String from;
    private String to;
    private long distance;
    private LocalTime time;
}