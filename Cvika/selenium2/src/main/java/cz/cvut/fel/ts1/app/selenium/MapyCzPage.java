package cz.cvut.fel.ts1.app.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class MapyCzPage {
    private WebDriver driver;
    private String url = "https://en.mapy.cz/zakladni?planovani-trasy";

    public MapyCzPage(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get(url);
    }

    public Route getRoute(String start, String end) {
        driver.findElement(By.xpath("//input[@placeholder='Enter a start']"))
                .sendKeys(start+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".route-item-point")));
        driver.findElement(By.xpath("//input[@placeholder='Enter an end']"))
                .sendKeys(end+ Keys.TAB);
        new WebDriverWait(driver, 10)
                .until(driver -> driver.findElement(By.cssSelector(".highligh-time")));

        //TODO: parse time and distance correctly!
        String time = driver.findElement(By.cssSelector(".highligh-time > .time"))
                .getText().split(" ")[0];
        String distance = driver.findElement(By.cssSelector(".highligh-time > .distance"))
                .getText().split(" ")[0];

        //convert to meters
        long d = (long)(Float.parseFloat(distance) * 1000);
        LocalTime t = LocalTime.parse(time, DateTimeFormatter.ofPattern("HH:mm"));
        return new Route(start, end, d, t);
    }
}
