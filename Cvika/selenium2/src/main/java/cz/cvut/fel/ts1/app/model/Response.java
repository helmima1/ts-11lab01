package cz.cvut.fel.ts1.app.model;
import lombok.Data;

@Data
public class Response {
    private String name = "ahoj";
    private int age = 2;
}
