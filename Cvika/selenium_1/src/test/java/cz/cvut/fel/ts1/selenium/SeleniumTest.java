package cz.cvut.fel.ts1.selenium;

import cz.cvut.fel.ts1.selenium.config.DriverFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class SeleniumTest {

    private static WebDriver driver;

    @BeforeAll
    public static void getDriver() throws IOException {
        driver = new DriverFactory().getDriver();
        //driver.findElement(By.tagName("div")); //Můžu hledat podle různých selectorů
        //driver.findElement(By.cssSelector("div"));
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_firstName")));
        //new By.ByCssSelector("div").findElement(driver)
        selectDestination.selectByIndex(2);
        Assertions.assertEquals("London", selectDestination);

    }

    @Test
    public void test2() {
        driver.get("https://flight-order.herokuapp.com");
    }

    @Test
    public void test1() {
        String title = driver.findElement(By.cssSelector("h1")).getText();
        driver.findElement(By.id("flight_form_firstName")).sendKeys("Honza");
        Select selectDestination = new Select(driver.findElement(By.id("flight_form_firstName")));
        //new By.ByCssSelector("div").findElement(driver)
        selectDestination.selectByIndex(2);
        driver.findElement(By.name("flight_form"));

        Assertions.assertEquals("London", selectDestination);
    }

    @AfterAll
    public static void quitDriver() {
        driver.quit();
    }

}
