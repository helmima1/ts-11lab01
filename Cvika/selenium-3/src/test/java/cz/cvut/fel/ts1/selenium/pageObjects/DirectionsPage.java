package cz.cvut.fel.ts1.selenium.pageObjects;

import lombok.Getter;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebElement;

@Getter
@DefaultUrl("https://en.mapy.cz/zakladni?planovani-trasy")
public class DirectionsPage extends PageObject {

    @FindBy(xpath = "//input[@placeholder='Enter a start']")
    WebElement startInput;

    @FindBy(xpath = "//input[@placeholder='Enter an end']")
    WebElement endInput;

}
