package cz.cvut.fel.ts1.refractoring;

import cz.cvut.fel.ts1.refactoring.Mail;
import org.junit.jupiter.api.Test;

import static cz.cvut.fel.ts1.refactoring.DBManager.findMail;

public class MailHelperTest {

    @Test
    public void createAndSendEmail() {
        //arrange
        int id = createAndSendEmail("nfsl@afd.ff");
        Mail m = findMail(id);
        //assert
        assertTrue(m);
    }
}
