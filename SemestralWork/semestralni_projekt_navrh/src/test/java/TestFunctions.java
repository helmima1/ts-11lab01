import com.sun.source.tree.AssertTree;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Animal;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Rabbit;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Wolf;
import cz.cvut.fel.semestralni_projekt_navrh.Entity;
import cz.cvut.fel.semestralni_projekt_navrh.EntityContainer;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Grass;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.NightShade;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Plant;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Tree;
import cz.cvut.fel.semestralni_projekt_navrh.SimulationField;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestFunctions {

    //ARRANGE
    static SimulationField simulationField;
    static int size = 3;
    static int numberOfGrass;
    static int numberOfTrees;
    static int numberOfNightShades;
    static int numberOfRabbits;
    static int numberOfWolves;
    static int maxFruits = 10;
    static int maxFruitsNightShades = 2;
    static int wolvesInitialSatiety = 10;
    static int rabbitsInitialSatiety = 10;
    static int speedOfGettingHungryRabbit = 3;
    static int speedOfGettingHungryWolf = 3;
    static double simulationSpeed = 1;

    @BeforeAll
    public static void initSimulation() {
        simulationField = new SimulationField(size, simulationSpeed);
        numberOfGrass = 1;
        numberOfTrees = 1;
        numberOfNightShades = 1;
        numberOfRabbits = 1;
        numberOfWolves = 1;
    }

    @Test
    /**
     * This function tests, if the list of plants were initialized with the right values. Tested values are their types, activity and coordinates.
     */
    public void createPlantsTest() {
        // --- ARRANGE ---
        size = 3;
        numberOfGrass = 1;
        numberOfTrees = 1;
        numberOfNightShades = 1;

        // --- ACT ---
        simulationField.createPlants(numberOfTrees, numberOfGrass, numberOfNightShades, maxFruits, maxFruitsNightShades);

        // --- ASSERT ---
        // Testing, if values in the listOfAnimals are of the right EntityType
        Assertions.assertEquals(Entity.EntityTypes.TREE, simulationField.getListOfPlants()[0].type);
        Assertions.assertEquals(Entity.EntityTypes.NIGHT_SHADE, simulationField.getListOfPlants()[1].type);
        Assertions.assertEquals(Entity.EntityTypes.GRASS, simulationField.getListOfPlants()[2].type);
        // Testing, if values in the listOfAnimals are acitve (not dead at the initialization)
        Assertions.assertTrue(simulationField.getListOfPlants()[0].getActive());
        Assertions.assertTrue(simulationField.getListOfPlants()[1].getActive());
        Assertions.assertTrue(simulationField.getListOfPlants()[2].getActive());
        // Testing, if coordinates are withing the bounds (lower than size of simulation field)
        Assertions.assertTrue(simulationField.getListOfPlants()[0].getXCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[0].getYCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[0].getXCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfPlants()[0].getYCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfPlants()[1].getXCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[1].getYCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[1].getXCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfPlants()[1].getYCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfPlants()[2].getXCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[2].getYCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfPlants()[2].getXCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfPlants()[2].getYCoordinates() >= 0);

    }

    @Test
    /**
     * This function tests, if the list of animals were initialized with the right values. Tested values are their types, activity and coordinates.
     */
    public void createAnimalsTest() {
        // --- ARRANGE ---
        size = 3;
        numberOfRabbits = 1;
        numberOfWolves = 1;

        // --- ACT ---
        simulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit);

        // --- ASSERT ---
        // Testing, if values in the listOfAnimals are of the right EntityType
        Assertions.assertEquals(Entity.EntityTypes.RABBIT, simulationField.getListOfAnimals()[0].type);
        Assertions.assertEquals(Entity.EntityTypes.WOLF, simulationField.getListOfAnimals()[1].type);
        // Testing, if values in the listOfAnimals are acitve (not dead at the initialization)
        Assertions.assertTrue(simulationField.getListOfAnimals()[0].getActive());
        Assertions.assertTrue(simulationField.getListOfAnimals()[1].getActive());
        // Testing, if coordinates are withing the bounds (lower than size of simulation field)
        Assertions.assertTrue(simulationField.getListOfAnimals()[0].getXCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfAnimals()[0].getYCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfAnimals()[0].getXCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfAnimals()[0].getYCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfAnimals()[1].getXCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfAnimals()[1].getYCoordinates() < size);
        Assertions.assertTrue(simulationField.getListOfAnimals()[1].getXCoordinates() >= 0);
        Assertions.assertTrue(simulationField.getListOfAnimals()[1].getYCoordinates() >= 0);
    }

    @Test
    void testEntityContainer() throws Exception {
        // Mockuji listOfAnimals, kde budou 3 hodnoty
        // Otestuji všechny funkce v EntityCOntaineru
        SimulationField mockedSimulationField = mock(SimulationField.class);
        Plant[] listOfPlants = new Plant[] {new Tree(new int[]{0, 0}), new Grass(5, new int[]{1, 0})};
        Animal[] listOfAnimals = new Animal[2];
        listOfAnimals = new Animal[] {new Rabbit(new int[]{1, 1}, listOfAnimals,0, simulationSpeed,rabbitsInitialSatiety, speedOfGettingHungryRabbit), new Wolf(new int[]{0, 1}, listOfAnimals,1, simulationSpeed,wolvesInitialSatiety, speedOfGettingHungryWolf)};
        when(mockedSimulationField.createPlants(numberOfTrees, numberOfGrass, numberOfNightShades, maxFruits, maxFruitsNightShades)).thenReturn(listOfPlants);
        when(mockedSimulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit)).thenReturn(listOfAnimals);

        EntityContainer entityContainer = new EntityContainer();
        EntityContainer entityContainer2 = new EntityContainer();

        String expectedErrorMessage = "";

        // ACT
        entityContainer.setListValue(mockedSimulationField.createPlants(numberOfTrees, numberOfGrass, numberOfNightShades, maxFruits, maxFruitsNightShades)[0]);
        try {
            entityContainer.setListValue(mockedSimulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit)[0]);
        } catch(Exception ex) {
            expectedErrorMessage = ex.getMessage();
        }
        entityContainer2.setListValue(mockedSimulationField.createPlants(numberOfTrees, numberOfGrass, numberOfNightShades, maxFruits, maxFruitsNightShades)[1]);
        entityContainer2.setListValue(mockedSimulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit)[1]);


        // ASSERT getObjectWithHighestPriority()
        Assertions.assertTrue(expectedErrorMessage.contains("ERROR: Cannot insert value into Entity container, because field with selected importance is full."));
        Assertions.assertEquals(entityContainer.getObjectWithHighestPriority().type, Entity.EntityTypes.TREE);
        Assertions.assertEquals(entityContainer2.getObjectWithHighestPriority().type, Entity.EntityTypes.WOLF);

        // Assert updateValue()
        // updatnu value u entityContainer2 na králíka
        entityContainer2.updateListValue(mockedSimulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit)[0]);
        Assertions.assertEquals(entityContainer2.getObjectWithHighestPriority().type, Entity.EntityTypes.RABBIT);
    }

}
