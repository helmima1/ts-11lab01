import cz.cvut.fel.semestralni_projekt_navrh.Animals.Animal;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Rabbit;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Wolf;
import cz.cvut.fel.semestralni_projekt_navrh.Entity;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Grass;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.NightShade;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Plant;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Tree;
import cz.cvut.fel.semestralni_projekt_navrh.SimulationField;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestProcess {

    static SimulationField simulationField;
    static int size = 2;
    static int maxFruits = 1;
    static int maxFruitsNightShades = 2;
    static int wolvesInitialSatiety = 10;
    static int rabbitsInitialSatiety = 10;
    static int speedOfGettingHungryRabbit = 3;
    static double simulationSpeed = 1;

    Entity[] fieldsToEat;
    Entity fieldToEat;

    @Test
    void testRabbitFindsNonPoisonousFood() throws Exception {
        // --- INITIALIZATION OF TEST CASE ---
        simulationField = new SimulationField(size, simulationSpeed);
        // Creating animals and plants suitable for testing
        Plant[] listOfPlants = new Plant[] {new Tree(new int[]{0, 0}), new Grass(maxFruits, new int[]{1, 1})};
        Animal[] listOfAnimals = new Animal[1];
        listOfAnimals = new Animal[] {new Rabbit(new int[]{1, 0}, listOfAnimals,0, simulationSpeed,rabbitsInitialSatiety, speedOfGettingHungryRabbit)};
        simulationField.setListOfPlants(listOfPlants);
        simulationField.setListOfAnimals(listOfAnimals);
        simulationField.initializeSFContainers();
        simulationField.listOfAnimals[0].setSimulationFieldPointer(simulationField.currentStateOfSFContainers);

        // --- SCOUT FOR FOOD ---
        // ACT
        fieldsToEat = simulationField.listOfAnimals[0].scoutForFood();
        fieldToEat = simulationField.listOfAnimals[0].findRandomFood(fieldsToEat);

        // Assert
        Assertions.assertEquals(null, fieldsToEat[1]);
        Assertions.assertEquals(null, fieldsToEat[2]);
        Assertions.assertEquals(null, fieldsToEat[3]);
        Assertions.assertEquals(null, fieldsToEat[4]);

        Assertions.assertEquals(Entity.EntityTypes.GRASS, fieldsToEat[0].type);
        Assertions.assertEquals(Entity.EntityTypes.GRASS, fieldToEat.type);
        // Tyto hodnoty si pak přeberou testEatNotPoisenousFood a testEatPoisenousFood

        // --- EAT FOOD ---
        // Arrange
        int currentSatiety = simulationField.listOfAnimals[0].getSatiety();
        int currentFruitsCount = Entity.convertEntityToPlantWithFruits(fieldToEat).getNumberOfFruits();
        // Act
        Entity.convertEntityToRabbit(simulationField.listOfAnimals[0]).eat(fieldToEat);

        // Assert
        Assertions.assertEquals(currentSatiety+1, simulationField.listOfAnimals[0].getSatiety());
        Assertions.assertEquals(currentFruitsCount-1, Entity.convertEntityToPlantWithFruits(fieldToEat).getNumberOfFruits());
    }

    @Test
    void testRabbitFindsPoisonousFood() throws Exception {
        // --- INITIALIZATION OF A TEST CASE ---
        simulationField = new SimulationField(size, simulationSpeed);
        // Creating animals and plants suitable for testing
        Plant[] listOfPlants = new Plant[] {new Tree(new int[]{0, 0}), new NightShade(maxFruitsNightShades, new int[]{1, 1})};
        Animal[] listOfAnimals = new Animal[1];
        listOfAnimals = new Animal[] {new Rabbit(new int[]{1, 0}, listOfAnimals,0, simulationSpeed,rabbitsInitialSatiety, speedOfGettingHungryRabbit)};
        simulationField.setListOfPlants(listOfPlants);
        simulationField.setListOfAnimals(listOfAnimals);
        simulationField.initializeSFContainers();
        simulationField.listOfAnimals[0].setSimulationFieldPointer(simulationField.currentStateOfSFContainers);

        // --- SCOUT FOR FOOD ---
        // ACT
        fieldsToEat = simulationField.listOfAnimals[0].scoutForFood();
        fieldToEat = simulationField.listOfAnimals[0].findRandomFood(fieldsToEat);

        // Assert
        Assertions.assertEquals(null, fieldsToEat[1]);
        Assertions.assertEquals(null, fieldsToEat[2]);
        Assertions.assertEquals(null, fieldsToEat[3]);
        Assertions.assertEquals(null, fieldsToEat[4]);

        Assertions.assertEquals(Entity.EntityTypes.NIGHT_SHADE, fieldsToEat[0].type);
        Assertions.assertEquals(Entity.EntityTypes.NIGHT_SHADE, fieldToEat.type);
        // Tyto hodnoty si pak přeberou testEatNotPoisenousFood a testEatPoisenousFood

        // --- EAT FOOD ---
        // Arrange
        // Act
        int currentNuberOfFruits = Entity.convertEntityToPlantWithFruits(fieldToEat).getNumberOfFruits();
        Entity.convertEntityToRabbit(simulationField.listOfAnimals[0]).eat(fieldToEat);

        // Assert
        Assertions.assertEquals(false, simulationField.listOfAnimals[0].getActive());
        Assertions.assertEquals(currentNuberOfFruits-1, Entity.convertEntityToPlantWithFruits(fieldToEat).getNumberOfFruits());
    }

    @Test
    void testRabbitDoesntFindFood() throws Exception {
        // --- INITIALIZATION OF A TEST CASE ---
        simulationField = new SimulationField(size, simulationSpeed);
        // Creating animals and plants suitable for testing
        Plant[] listOfPlants = new Plant[] {new Tree(new int[]{0, 0})};
        Animal[] listOfAnimals = new Animal[1];
        listOfAnimals = new Animal[] {new Rabbit(new int[]{1, 0}, listOfAnimals,0, simulationSpeed,rabbitsInitialSatiety, speedOfGettingHungryRabbit)};
        simulationField.setListOfPlants(listOfPlants);
        simulationField.setListOfAnimals(listOfAnimals);
        simulationField.initializeSFContainers();
        simulationField.listOfAnimals[0].setSimulationFieldPointer(simulationField.currentStateOfSFContainers);

        // --- SCOUT FOR FOOD ---
        // ACT
        String errorMessageFoodNull = "";

        fieldsToEat = simulationField.listOfAnimals[0].scoutForFood();
        fieldToEat = simulationField.listOfAnimals[0].findRandomFood(fieldsToEat);

        // Assert
        Assertions.assertEquals(null, fieldsToEat[0]);
        Assertions.assertEquals(null, fieldsToEat[1]);
        Assertions.assertEquals(null, fieldsToEat[2]);
        Assertions.assertEquals(null, fieldsToEat[3]);
        Assertions.assertEquals(null, fieldsToEat[4]);

        Assertions.assertEquals(null, fieldToEat);
    }

    @Test
    void testMoveRabbitPossible() throws Exception {
        // --- INITIALIZATION OF A TEST CASE ---
        simulationField = new SimulationField(size, simulationSpeed);
        // Creating animals and plants suitable for testing
        Plant[] listOfPlants = new Plant[] {new Tree(new int[]{0, 0}), new Tree(new int[]{0, 1})};
        Animal[] listOfAnimals = new Animal[1];
        listOfAnimals = new Animal[] {new Rabbit(new int[]{1, 0}, listOfAnimals,0, simulationSpeed,rabbitsInitialSatiety, speedOfGettingHungryRabbit)};
        simulationField.setListOfPlants(listOfPlants);
        simulationField.setListOfAnimals(listOfAnimals);
        simulationField.initializeSFContainers();
        simulationField.listOfAnimals[0].setSimulationFieldPointer(simulationField.currentStateOfSFContainers);
        Entity.convertEntityToRabbit(simulationField.listOfAnimals[0]).setPointerToItsList(listOfAnimals);

        // --- FIND PLACE TO MOVE ---

        int[][] fieldsToMove = simulationField.listOfAnimals[0].scoutForPlacesToMove();
        int[] fieldToMove = simulationField.listOfAnimals[0].findRandomPlaceToMove(fieldsToMove);

        //Assertions
        Assertions.assertEquals(-1, fieldsToMove[1][0]);
        Assertions.assertEquals(-1, fieldsToMove[1][1]);
        Assertions.assertEquals(-1, fieldsToMove[2][0]);
        Assertions.assertEquals(-1, fieldsToMove[2][1]);
        Assertions.assertEquals(-1, fieldsToMove[3][0]);
        Assertions.assertEquals(-1, fieldsToMove[3][1]);

        Assertions.assertEquals(1, fieldsToMove[0][0]);
        Assertions.assertEquals(1, fieldsToMove[0][1]);

        Assertions.assertEquals(1, fieldsToMove[0][0]);
        Assertions.assertEquals(1, fieldsToMove[0][1]);

        // --- MOVING TO THE FREE PLACE ---

        // Act
        Entity.convertEntityToRabbit(simulationField.listOfAnimals[0]).move(fieldToMove);

        // Assertions
        // Rabbit has the right coordinates
        Assertions.assertEquals(simulationField.listOfAnimals[0].getXCoordinates(), fieldToMove[0]);
        Assertions.assertEquals(simulationField.listOfAnimals[0].getYCoordinates(), fieldToMove[1]);
        // Rabbit is on the new position in the currentStateOfSFContainers
        Assertions.assertEquals(Entity.EntityTypes.PLAIN_FIELD, simulationField.currentStateOfSFContainers[1][0].getObjectWithHighestPriority().type);
        // Rabbit is not on the old position in the currentStateOfSFFields
        Assertions.assertEquals(Entity.EntityTypes.RABBIT, simulationField.currentStateOfSFContainers[1][1].getObjectWithHighestPriority().type);
    }
}
