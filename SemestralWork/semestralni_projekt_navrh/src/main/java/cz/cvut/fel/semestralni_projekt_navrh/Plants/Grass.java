/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Plants;

/**
 *
 * @author martik
 */
public class Grass extends Plant{

    public Grass(int numberOfFruits, int[] coordinates) {
        super(numberOfFruits, coordinates);
        this.imagePath = "semestralni_projekt_navrh/images/grass.png";
        this.eatable = true;
        this.viewImportance = 2;
        this.type = EntityTypes.GRASS;
        this.canBeSteppedOn = true;
        this.isFood = true;
    }
}
