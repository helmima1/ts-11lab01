package cz.cvut.fel.semestralni_projekt_navrh;

import cz.cvut.fel.semestralni_projekt_navrh.Animals.Animal;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Rabbit;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Plant;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public abstract class Entity {
    protected boolean active = true;
    protected int[] coordinates;
    protected String imagePath;
    protected int viewImportance;
    public Entity.EntityTypes type;
    static int maxViewImportance = 3;
    protected boolean canBeLayered = true;
    protected static Random rand = new Random();
    protected boolean canBeSteppedOn;
    protected boolean isFood;
    public ReentrantLock lock;

    public Entity(int[] coordinates) {
        this.coordinates = coordinates;
    }

    /**
     * Sets active = false;
     */
    public void die() {
        this.active = false;
    }

    /**
     * Entity types are asigned to each of the instances to distinguish the type.
     */
    public enum EntityTypes {
        PLAIN_FIELD,
        GRASS,
        TREE,
        NIGHT_SHADE,
        RABBIT,
        FINCH,
        WOLF
    }

    public int getXCoordinates() {
        return coordinates[0];
    }

    public int getYCoordinates() {
        return coordinates[1];
    }

    public int[] getCoordinates() {
        return coordinates;
    }

    public boolean getActive() {
        return active;
    }

    public void setCoordinates(int[] coordinates) {
        this.coordinates = coordinates;
    }

    public void setLock(ReentrantLock lock) {
        this.lock = lock;
    }

    // --- STATIC FUNCTIONS TO CONVERT TO ENTITY TO ITS CHILD TYPES TO ENSURE ACCESS TO ITS VARIABLES AND METHODS ---

    /**
     * Converts entity to plant, if its type is either grass or night shade.
     * @param entity Entity
     * @return Object of type Plant if conditions are met. Otherwise it returns null.
     */
    static public Plant convertEntityToPlantWithFruits(Entity entity) {
        if(entity.type == Entity.EntityTypes.GRASS || entity.type == Entity.EntityTypes.NIGHT_SHADE) {
            return (Plant) entity;
        } else {
            return null;
        }
    }

    /**
     * Converts entity to Animal, if possible.
     * @param entity Entity
     * @return Object of type Animal if conditions are met. Otherwise it returns null.
     */
    static public Animal convertEntityToAnimal(Entity entity) {
        if(entity.type == EntityTypes.RABBIT || entity.type == Entity.EntityTypes.WOLF) {
            return (Animal) entity;
        } else {
            return null;
        }
    }

    /**
     * Converts entity to Rabbit, if possible.
     * @param entity Entity
     * @return Object of type Rabbit if conditions are met. Otherwise it returns null.
     */
    static public Rabbit convertEntityToRabbit(Entity entity) {
        if(entity.type == EntityTypes.RABBIT) {
            return (Rabbit) entity;
        } else {
            return null;
        }
    }

}
