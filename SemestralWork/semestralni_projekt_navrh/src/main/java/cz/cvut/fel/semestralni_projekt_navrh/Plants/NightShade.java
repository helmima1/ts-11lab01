/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Plants;
import cz.cvut.fel.semestralni_projekt_navrh.Entity.EntityTypes;

/**
 *
 * @author martik
 */
public class NightShade extends Plant {

    public NightShade(int numberOfFruits, int[] coordinates) {
        super(numberOfFruits, coordinates);
        this.imagePath = "semestralni_projekt_navrh/images/night-shade.png";
        this.type = EntityTypes.NIGHT_SHADE;
        this.isFood = true;
    }
}
