package cz.cvut.fel.semestralni_projekt_navrh;

import cz.cvut.fel.semestralni_projekt_navrh.Animals.Rabbit;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Plant;

import static cz.cvut.fel.semestralni_projekt_navrh.Main.LOG;

/**
 * This class is used to contain multiple Entities on one coordinate, for example grass on the plain field or wolf in grass on the plain field.
 */
public class EntityContainer {
    Entity[] list = new Entity[Entity.maxViewImportance];
    int entityContainerID;
    static int numberOfContainers = 1;

    public EntityContainer() {
        this.entityContainerID = numberOfContainers;
        numberOfContainers += 1;
        LOG.info("New container with id "+entityContainerID+" created.");
    }

    /**
     * Insert value into list distinguished by viewImportance.
     * @param entity
     */
    public void setListValue(Entity entity) throws Exception {
        if(list[entity.viewImportance-1] == null) {
            list[entity.viewImportance-1] = entity;
            LOG.fine("New value of importance "+entity.viewImportance+" and type "+entity.imagePath+" added to the container of id "+entityContainerID+".");
        } else {
            System.out.println("ERROR: Cannot insert value into Entity container, because field with selected importance is full.");
            throw new Exception("ERROR: Cannot insert value into Entity container, because field with selected importance is full.");
        }
    }

    /**
     * Updates value in the list or creating it, if there is none.
     * @param entity Selected Entity
     */
    public void updateListValue(Entity entity) {
        list[entity.viewImportance-1] = entity;
    }

    /**
     * Gets and Entity out of the list in the EntityContainer, that are indexed by the viewImportance.
     * @param viewImportance viewImportance of desired entity
     * @return Diesired Entity, otherwise null.
     */
    public Entity getEntityByViewImportance(int viewImportance) {
        return list[viewImportance-1];
    }

    /**
     * This function returns object with highest priority. There allways has to be one, otherewise exception raises.
     * @return
     */
    public Entity getObjectWithHighestPriority() {
        for (int i = 0; i < list.length; i++) {
            if(list[i] != null) {
                return list[i];
            }
        }
        return null;
    }

    /**
     * This function lists through a whole container to look for entities, that can be stepped on.
     * @return True, if can be stepped on; False, if can be stepped on.
     */
    public boolean canBeSteppedOn() {
        for (int i = 0; i < list.length; i++) {
            if(list[i] != null) {
                if (!list[i].canBeSteppedOn) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * This function works directly  with isEatableForThisAnimal() in Animal, which determins, if the Entity is edible for the specified entity.
     *
     * @return Entity, if its edible; null, if it is not edible.
     */
    public Entity isEatable() {
        for (int i = 0; i < list.length; i++) {
            if(list[i] != null) {
                if (list[i].isFood) {
                    return list[i];
                }
            }
        }
        return null;
    }

    public Entity[] getList() {
        return this.list;
    }

    /**
     * Removes entity, if possible.
     * @param entity Entity chosen to be removed.
     */
    public void removeEntity(Entity entity) {
        if (list[entity.viewImportance-1] != null) {
            list[entity.viewImportance - 1] = null;
        } else {
            LOG.warning("There is nothing to remove.");
        }
    }

    private void printListForDebugging() {
        for (int i = 0; i < this.list.length; i++) {
            if (list[i] != null) {
                System.out.println("list.type is on position "+i+" is " + list[i].type);
            }
        }
        System.out.println("Container list: "+list[0]+list[1]+list[2]);
    }
}
