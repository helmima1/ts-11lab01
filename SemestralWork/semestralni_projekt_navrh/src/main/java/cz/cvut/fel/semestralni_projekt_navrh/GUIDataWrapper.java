package cz.cvut.fel.semestralni_projekt_navrh;

import static cz.cvut.fel.semestralni_projekt_navrh.Main.LOG;

public class GUIDataWrapper {
    public String imageAddress;
    public boolean hasFruits;
    public boolean hasSatiety;
    private int numberOfFruits;
    private int satiety;

    public GUIDataWrapper(String imageAddress, boolean hasFruits, boolean hasSatiety) {
        this.imageAddress = imageAddress;
        this.hasFruits = hasFruits;
        this.hasSatiety = hasSatiety;
    }

    public void setNumberOfFruits(int numberOfFruits) {
        if(hasFruits) {
            this.numberOfFruits = numberOfFruits;
        } else {
            LOG.warning("Trying to insert fruit where hasFruit is false. Image adress is: "+this.imageAddress);
        }
    }

    public void setSatiety(int satiety) {
        if(hasSatiety) {
            this.satiety = satiety;
        } else {
            LOG.warning("Trying to insert satiety where hasSatiety is false. Image adress is: "+this.imageAddress);
        }
    }

    /**
     * Returns number of fruits, if there are fruits, otherwise returns -1.
     * @return
     */
    public int getNumberOfFruits() {
        if (hasFruits) {
            return this.numberOfFruits;
        } else {
            return -1;
        }
    }

    /**
     * Returns satiety, if its possible, otherwise returns -1.
     * @return
     */
    public int getSatiety() {
        if (hasSatiety) {
            return this.satiety;
        } else {
            return -1;
        }
    }
}
