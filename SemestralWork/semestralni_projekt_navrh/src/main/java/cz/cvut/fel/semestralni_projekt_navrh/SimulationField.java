/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh;

import cz.cvut.fel.semestralni_projekt_navrh.Animals.Animal;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Rabbit;
import cz.cvut.fel.semestralni_projekt_navrh.Animals.Wolf;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Grass;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.NightShade;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Plant;
import cz.cvut.fel.semestralni_projekt_navrh.Plants.Tree;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import static cz.cvut.fel.semestralni_projekt_navrh.Main.LOG;

/**
 *
 * @author martik
 */
public class SimulationField {
    private int sizeX;
    private int sizeY;
    public double simulationSpeed;
    public Plant[] listOfPlants;
    public Animal[] listOfAnimals;
    private PlainField[] listOfPlainFields;
    public EntityContainer[][] currentStateOfSFContainers;
    private int maxFruits;
    private int maxFruitsNightShades;
    public ReentrantLock lock;


    /**
     * Initialization of Simulation field
     *
     * @param size Size of the field.
     * @param simulationSpeed Value of simulation speed.
     */
    public SimulationField(int size, double simulationSpeed) {
        this.sizeX = size;
        this.sizeY = size;
        this.simulationSpeed = simulationSpeed;
        this.lock = new ReentrantLock(); // Creating lock
        this.fillRestOfTheFieldWithPlainField(); //filling rest of the field with PlainField
        // --- LOGS ---
        LOG.info("SimulationField initialized with following values:: sizeX: "+sizeX+", sizeY: "+sizeY+", simulation speed: "+simulationSpeed);
    }

    /**
     * Tests if all animals are active or not.
     * @return True, if all entities active variable is set to false; False, if at least one entity is active.
     */
    public boolean areAllAnimalsAreDead() {
        for (int i = 0; i < listOfAnimals.length; i++) {
            if(listOfAnimals[i].active) {
                LOG.finest("At least one animal is not dead");
                return false;
            }
        }
        LOG.finest("All animals are dead.");
        return true;
    }

    public void setListOfPlants(Plant[] plants) {
        this.listOfPlants = plants;
    }

    public void setListOfAnimals(Animal[] animals) {
        this.listOfAnimals = animals;
    }

    /**
     * This function creates statistics from the listOfAnimals and listOfPlants.
     *
     * @return List of ints, on each index is count of following things: [0] = active trees, [1] = active grass, [2] = active night shades, [3] = active rabbits, [4] = active wolves, [5] = average satiety, [6] = rabbits that were killed by a wolf, [7] = rabbits, that died of hunger.
     */
    public int[] generateStatistics() {
        LOG.info("INITIATED: getCountOfAllEntities()");
        int[] result = new int[8];
        for (int i = 0; i < result.length; i++) {
            result[i] = 0;
        }
        for (int i = 0; i < listOfPlants.length; i++) {
            if (listOfPlants[i].type == Entity.EntityTypes.TREE && listOfPlants[i].active) {
                result[0] += 1;
            }
            if (listOfPlants[i].type == Entity.EntityTypes.GRASS && listOfPlants[i].active) {
                result[1] += 1;
            }
            if (listOfPlants[i].type == Entity.EntityTypes.NIGHT_SHADE && listOfPlants[i].active) {
                result[2] += 1;
            }
        }
        int satiety = 0;
        for (int i = 0; i < listOfAnimals.length; i++) {
            if (listOfAnimals[i].type == Entity.EntityTypes.RABBIT && listOfAnimals[i].active) {
                result[3] += 1;
                satiety += listOfAnimals[i].getSatiety();
            }
            if (listOfAnimals[i].type == Entity.EntityTypes.WOLF && listOfAnimals[i].active) {
                result[4] += 1;
                satiety += listOfAnimals[i].getSatiety();
            }
            if (listOfAnimals[i].type == Entity.EntityTypes.RABBIT && Entity.convertEntityToRabbit(listOfAnimals[i]).killedByWolf && !listOfAnimals[i].getActive()) {
                result[6] += 1;
            }
            if (listOfAnimals[i].type == Entity.EntityTypes.RABBIT && !Entity.convertEntityToRabbit(listOfAnimals[i]).killedByWolf && !Entity.convertEntityToRabbit(listOfAnimals[i]).killedByNightShade && !listOfAnimals[i].getActive() && !listOfAnimals[i].getActive()) {
                result[7] += 1;
            }
        }
        if (result[3] + result[4] == 0) {
            satiety = 0;
        } else {
            satiety = satiety / (result[3] + result[4]);
        }
        result[5] = satiety;
        return result;
    }

    /**
     * This function creates starts all threads of the simulation. It is called from outside, not in initializing simulation field!
     */
    public void startSimulationThreads() {
        LOG.info("INITIATED: simulationThreads()");
        for (int i = 0; i < listOfAnimals.length; i++) {
            LOG.fine("Starting new animal with id"+Entity.convertEntityToAnimal(listOfAnimals[i]).getAnimalID()+" thread.");
            listOfAnimals[i].setSimulationFieldPointer(this.currentStateOfSFContainers);
            listOfAnimals[i].setLock(this.lock);
            Thread newThread = new Thread((Runnable) listOfAnimals[i]);
            newThread.start();
        }
    }

    /**
     *This function determines, if listOfEntities contains certain Entity defined with coordinates.
     * @param listOfEntities List of Entity to test
     * @param coordinates Coordinates to find in this list of Entities
     * @return True, if coordinates found; False, if not.
     */
    private boolean containsCoordinates(Entity[] listOfEntities, int[] coordinates) {
        LOG.fine("INITIATED: containsCoordinates");
        if (listOfEntities == null) {
            return false;
        }
        boolean result = false;
        // Lists through all entities and testing given coordinates for them
        for (int i = 0; i < listOfEntities.length; i++) {
            if(listOfEntities[i] != null) {
                LOG.finest("Currently testing for object "+listOfEntities[i].type+" with coordinates "+Arrays.toString(listOfEntities[i].coordinates));
                LOG.finest("Tested coordinates are: "+Arrays.toString(coordinates));
                if(listOfEntities[i].getXCoordinates() == coordinates[0] && listOfEntities[i].getYCoordinates() == coordinates[1]) {
                    LOG.finest("containsCoordinates RETURNED TRUE");
                    return true;
                }
            }
        }
        LOG.finest("containsCoordinates RETURNED FALSE");
        return result;
    }

    public int[] createRandomCoordinates(int bounds) {
        Random rand = new Random();
        return new int[] {rand.nextInt(0, bounds), rand.nextInt(0, bounds)};
    }

    /**
     * This function creates plants on random locations within the bounds of simulation field.
     * @param numberOfTrees number of trees
     * @param numberOfGrass number of grass
     * @param numberOfNightShades number of night shades
     */
    public Plant[] createPlants(int numberOfTrees, int numberOfGrass, int numberOfNightShades, int maxFruits, int maxFruitsNightShades) {
        this.listOfPlants = new Plant[numberOfGrass + numberOfTrees + numberOfNightShades];
        LOG.info("INITIATED: createPlantsField()");
        this.maxFruits = maxFruits;
        this.maxFruitsNightShades = maxFruitsNightShades;
        LOG.finer("Creating trees started.");
        Random rand = new Random();
        for (int i = 0; i < numberOfTrees; i++) {
            while(true) {
                // Generating random coordinates and testing if they are not already in listOfPlants. If not, than it creates plant on that coordinates
                int[] coordinates = createRandomCoordinates(sizeX);
                if (!containsCoordinates(listOfPlants, coordinates) && !containsCoordinates(listOfAnimals, coordinates)) {
                    listOfPlants[i] = new Tree(coordinates);
                    LOG.fine("Created new Tree on coordinates: "+Arrays.toString(listOfPlants[i].coordinates));
                    break;
                }
            }
        }
        LOG.finer("Creating night shades started.");
        for (int i = numberOfTrees; i < numberOfTrees+numberOfNightShades; i++) {
            while(true) {
                // Generating random coordinates and testing if they are not already in listOfPlants. If not, than it creates plant on that coordinates
                int[] coordinates = createRandomCoordinates(sizeX);
                if (!containsCoordinates(listOfPlants, coordinates) && !containsCoordinates(listOfAnimals, coordinates)) {
                    listOfPlants[i] = new NightShade(rand.nextInt(1,this.maxFruitsNightShades), coordinates);
                    LOG.fine("Created new Night shade on coordinates: "+Arrays.toString(listOfPlants[i].coordinates));
                    break;
                }
            }
        }
        LOG.finer("Creating grass started.");
        for (int i = numberOfTrees+numberOfNightShades; i < numberOfTrees+numberOfNightShades+numberOfGrass; i++) {
            while(true) {
                // Generating random coordinates and testing if they are not already in listOfPlants. If not, than it creates plant on that coordinates
                int[] coordinates = {rand.nextInt(0, sizeX), rand.nextInt(0, sizeY)};
                if (!containsCoordinates(listOfPlants, coordinates) && !containsCoordinates(listOfAnimals, coordinates)) {
                    listOfPlants[i] = new Grass(rand.nextInt(1, this.maxFruits), coordinates);
                    LOG.fine("Created new Grass on coordinates: "+Arrays.toString(listOfPlants[i].coordinates));
                    break;
                }
            }
        }
        return this.listOfPlants;
    }

    /**
     * This function creates animals on random locations within the bounds of simulation field.
     * @param numberOfWolves number of wolves
     * @param numberOfRabbits number of rabbits
     * @param wolvesInitialSatiety wolves initial satiety
     * @param rabbitsInitialSatiety rabbits initial satiety
     * @param speedOfGettingHungryWolves speed of getting hungry - wolves
     * @param speedOfGettingHungryRabbits speed of getting hungry - rabbits
     */
    public Animal[] createAnimals(int numberOfWolves, int numberOfRabbits, int wolvesInitialSatiety, int rabbitsInitialSatiety, int speedOfGettingHungryWolves, int speedOfGettingHungryRabbits) {
        this.listOfAnimals = new Animal[numberOfWolves + numberOfRabbits];
        Random rand = new Random();
        for (int i = 0; i < numberOfRabbits; i++) {
            while(true) {
                // Generating random coordinates and testing if they are not already in listOfPlants. If not, than it creates plant on that coordinates
                int[] coordinates = {rand.nextInt(0, sizeX), rand.nextInt(0, sizeY)};
                if (!containsCoordinates(listOfPlants, coordinates) && !containsCoordinates(listOfAnimals, coordinates)) {
                    // There is just plain field on the coordinates
                    listOfAnimals[i] = new Rabbit(coordinates, listOfAnimals, i, this.simulationSpeed, rabbitsInitialSatiety, speedOfGettingHungryRabbits);
                    LOG.fine("Created new Rabbit on coordinates: "+Arrays.toString(listOfAnimals[i].coordinates));
                    break;
                } else if (getEntityOutOfListByCoord(listOfPlants, coordinates) != null) {
                    //  There is plain field and grass on the same coordinates
                    if(getEntityOutOfListByCoord(listOfPlants, coordinates).type == Entity.EntityTypes.GRASS && !containsCoordinates(listOfAnimals, coordinates)) {
                        listOfAnimals[i] = new Rabbit(coordinates, listOfAnimals, i, this.simulationSpeed, rabbitsInitialSatiety, speedOfGettingHungryRabbits);
                        LOG.fine("Created new Rabbit on coordinates: "+Arrays.toString(listOfAnimals[i].coordinates)+" stacked od GRASS");
                        break;
                    }
                }
            }
        }
        for (int i = numberOfRabbits; i < numberOfRabbits+numberOfWolves; i++) {
            while(true) {
                // Generating random coordinates and testing if they are not already in listOfPlants. If not, than it creates plant on that coordinates
                int[] coordinates = {rand.nextInt(0, sizeX), rand.nextInt(0, sizeY)};
                if (!containsCoordinates(listOfPlants, coordinates) && !containsCoordinates(listOfAnimals, coordinates)) {
                    // There is just plain field on the coordinates
                    listOfAnimals[i] = new Wolf(coordinates, listOfAnimals, i, this.simulationSpeed, wolvesInitialSatiety, speedOfGettingHungryWolves);
                    LOG.fine("Created new Wolf on coordinates: "+Arrays.toString(listOfAnimals[i].coordinates));
                    break;
                } else if (getEntityOutOfListByCoord(listOfPlants, coordinates) != null) {
                    //  There is plain field and grass on the same coordinates
                    if(getEntityOutOfListByCoord(listOfPlants, coordinates).type == Entity.EntityTypes.GRASS && !containsCoordinates(listOfAnimals, coordinates)) {
                        listOfAnimals[i] = new Wolf(coordinates, listOfAnimals, i, this.simulationSpeed, wolvesInitialSatiety, speedOfGettingHungryWolves);
                        LOG.fine("Created new Wolf on coordinates: "+Arrays.toString(listOfAnimals[i].coordinates)+" stacked od GRASS");
                        break;
                    }
                }
            }
        }
        return this.listOfAnimals;
        //initializeSFContainers();
    }

    /**
     * This function takes an Entity from a list of Entity by its coordinates
     * @param listOfEntities list of Entity
     * @param coordinates Coordinates to search
     * @return Entity of desired coordinates if found, otherwise it returns null
     */
    private Entity getEntityOutOfListByCoord(Entity[] listOfEntities, int[] coordinates) {
        if (listOfEntities != null) {
            for (int i = 0; i < listOfEntities.length; i++) {
                if (listOfEntities[i] != null) {
                    if (listOfEntities[i].getXCoordinates() == coordinates[0] && listOfEntities[i].getYCoordinates() == coordinates[1]) {
                        return listOfEntities[i];
                    }
                } else {
                    LOG.warning("isTypeOnTheCoordinates: Found null value of an item in list while running.");
                }
            }
        }
        LOG.warning("isTypeOnTheCoordinates: There is no Entity with such coordinates in the array.");
        return null;
    }

    /**
     * This function converts currentStateOfSFContainers to grid of image adreses for GUI to display.
     * This function expects currentStateOfSFContainers to be fully initialized, i.e. none of the fields is null nor are the values instide a EntityContainer. 
     * @return GUIDataWrapper[][] - field of informations wrapped in GUIDataWrapper
     */
    public GUIDataWrapper[][] exportDataForGUI() throws Exception {
        LOG.info("INITIATED: exportDataForGUI()");
        GUIDataWrapper[][] returnData = new GUIDataWrapper[sizeX][sizeY];
        for (int i = 0; i < this.sizeX; i++) {
            for (int j = 0; j < this.sizeY; j++) {
                // Look at the highest priority Entity
                Entity highestPriorityEntity = currentStateOfSFContainers[i][j].getObjectWithHighestPriority();
                // Configures data for gui based on the type of the field
                if (highestPriorityEntity.active) {
                    switch (highestPriorityEntity.type) {
                        case PLAIN_FIELD:
                        case TREE:
                            returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, false, false);
                            break;
                        case GRASS:
                        case NIGHT_SHADE:
                            returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, true, false);
                            returnData[i][j].setNumberOfFruits(Entity.convertEntityToPlantWithFruits(highestPriorityEntity).getNumberOfFruits());
                            break;
                        case RABBIT:
                            //  Look, if there is also grass by asking for item with viewImportance + 1. If so, than display image of path images/wolf-grass.png or images/rabbit-grass.png
                            if (currentStateOfSFContainers[i][j].getEntityByViewImportance(2) != null) {
                                if (currentStateOfSFContainers[i][j].getEntityByViewImportance(2).type == Entity.EntityTypes.GRASS) {
                                    if (Entity.convertEntityToPlantWithFruits(currentStateOfSFContainers[i][j].getEntityByViewImportance(2)).active)  {
                                        returnData[i][j] = new GUIDataWrapper("semestralni_projekt_navrh/images/rabbit-grass.png", true, true);
                                        returnData[i][j].setNumberOfFruits(Entity.convertEntityToPlantWithFruits(currentStateOfSFContainers[i][j].getEntityByViewImportance(2)).getNumberOfFruits());
                                    } else {
                                        returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, false, true);
                                    }
                                    returnData[i][j].setSatiety(Entity.convertEntityToAnimal(highestPriorityEntity).getSatiety());
                                }
                            } else {
                                returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, false, true);
                                returnData[i][j].setSatiety(Entity.convertEntityToAnimal(highestPriorityEntity).getSatiety());
                            }
                            break;
                        case WOLF:
                            //  Look, if there is also grass by asking for item with viewImportance + 1. If so, than display image of path images/wolf-grass.png or images/wolf-grass.png
                            if (currentStateOfSFContainers[i][j].getEntityByViewImportance(2) != null) {
                                if (currentStateOfSFContainers[i][j].getEntityByViewImportance(2).type == Entity.EntityTypes.GRASS) {
                                    if (Entity.convertEntityToPlantWithFruits(currentStateOfSFContainers[i][j].getEntityByViewImportance(2)).active)  {
                                        returnData[i][j] = new GUIDataWrapper("semestralni_projekt_navrh/images/wolf-grass.png", true, true);
                                        returnData[i][j].setNumberOfFruits(Entity.convertEntityToPlantWithFruits(currentStateOfSFContainers[i][j].getEntityByViewImportance(2)).getNumberOfFruits());
                                    } else {
                                        returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, false, true);
                                    }
                                    returnData[i][j].setSatiety(Entity.convertEntityToAnimal(highestPriorityEntity).getSatiety());
                                }
                            } else {
                                returnData[i][j] = new GUIDataWrapper(highestPriorityEntity.imagePath, false, true);
                                returnData[i][j].setSatiety(Entity.convertEntityToAnimal(highestPriorityEntity).getSatiety());
                            }
                            break;
                        default:
                            LOG.warning("Type not found for " + highestPriorityEntity.type + " on coordinates " + i + " " + j);
                    }
                    //LOG.fine("A GUIDataWrapper on coordinates "+i+" "+j+" successfully created!");
                } else {
                    returnData[i][j] = new GUIDataWrapper("semestralni_projekt_navrh/images/nothing.png", false, false);
                }
            }
        }
        return returnData;
    }

    /**
     * This function creates Simulation field defined with Entity containers to ensure more than one Entity to be on one field.
     * This allows for example for wolf and grass to be on the same field because logically wolf can walk on the grass.
     */
    public void initializeSFContainers() throws Exception {
        LOG.info("INITIATED: initializeSFContainers()");
        currentStateOfSFContainers = new EntityContainer[sizeX][sizeY];
        // Inserts all plants
        for (int i = 0; i < listOfPlants.length; i++) {
            // Creates container if there is none
            if(currentStateOfSFContainers[listOfPlants[i].getXCoordinates()][listOfPlants[i].getYCoordinates()] == null) {
                currentStateOfSFContainers[listOfPlants[i].getXCoordinates()][listOfPlants[i].getYCoordinates()] = new EntityContainer();
            }
            // Adding plant into the EntityContainer
            currentStateOfSFContainers[listOfPlants[i].getXCoordinates()][listOfPlants[i].getYCoordinates()].setListValue(listOfPlants[i]);
            LOG.finer("Plant of id "+i+" was successfully added into an EntityContainer");
        }
        for (int i = 0; i < listOfAnimals.length; i++) {
            // Creates container if there is none
            if(currentStateOfSFContainers[listOfAnimals[i].getXCoordinates()][listOfAnimals[i].getYCoordinates()] == null) {
                currentStateOfSFContainers[listOfAnimals[i].getXCoordinates()][listOfAnimals[i].getYCoordinates()] = new EntityContainer();
            }
            // Adding Animal into the EntityContainer
            currentStateOfSFContainers[listOfAnimals[i].getXCoordinates()][listOfAnimals[i].getYCoordinates()].setListValue(listOfAnimals[i]);
            LOG.finer("Animal of id "+listOfAnimals[i].getAnimalID()+" was successfully added into an EntityContainer");
        }
        for (int i = 0; i < listOfPlainFields.length; i++) {
            // Creates container if there is none
            if(currentStateOfSFContainers[listOfPlainFields[i].getXCoordinates()][listOfPlainFields[i].getYCoordinates()] == null) {
                currentStateOfSFContainers[listOfPlainFields[i].getXCoordinates()][listOfPlainFields[i].getYCoordinates()] = new EntityContainer();
            }
            // Adding plain field into the EntityContainer
            currentStateOfSFContainers[listOfPlainFields[i].getXCoordinates()][listOfPlainFields[i].getYCoordinates()].setListValue(listOfPlainFields[i]);
            LOG.finer("Plain field of id "+i+" was successfully added into an EntityContainer");
        }
    }

    /**
     * This function creates list of plain fields, that fills remaining space in sizeX*sizeY grid.
     */
    public void fillRestOfTheFieldWithPlainField() {
        LOG.info("INITIATED: fillRestOfTheFieldWithPlain()");
        listOfPlainFields = new PlainField[(sizeX*sizeY)];
        LOG.fine("New listOfPlainFields[] of length "+((sizeX*sizeY))+" is set up.");
        int instanceID = 0;
        for (int i = 0; i < this.sizeX; i++) {
            for (int j = 0; j < this.sizeY; j++) {
                int[] coordinates = {i, j};
                listOfPlainFields[instanceID] = new PlainField(coordinates);
                LOG.finer("New PlainField on coordinates: "+coordinates[0]+" "+coordinates[1]+" with instanceID "+instanceID);
                instanceID += 1;
            }
        }
    }

    /**
     * Used for debugging purposes, prints all Entities in list of Entity.
     * @param entities listOfEntities
     */
    private void printListOfEntitiesForDebugging(Entity[] entities) {
        System.out.println("PRINTING ANIMALS");
        for (int i = 0; i < entities.length; i++) {
            System.out.println("Animal id "+i+" is on "+Arrays.toString(entities[i].coordinates));
        }
    }

    public int getSizeX() {
        return this.sizeX;
    }

    public int getSizeY() {
        return this.sizeY;
    }

    public Entity[] getListOfPlants() {
        return this.listOfPlants;
    }

    public Entity[] getListOfAnimals() {
        return this.listOfAnimals;
    }
}
