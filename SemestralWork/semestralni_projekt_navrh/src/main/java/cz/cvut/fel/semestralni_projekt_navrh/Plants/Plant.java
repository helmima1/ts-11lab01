/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Plants;

import cz.cvut.fel.semestralni_projekt_navrh.Entity;

/**
 *
 * @author martik
 */
public abstract class Plant extends Entity {
    protected boolean eatable;
    protected int numberOfFruits;

    public Plant(int numberOfFruits, int[] coordinates) {
        super(coordinates);
        this.numberOfFruits = numberOfFruits;
        this.viewImportance = 1;
        this.canBeSteppedOn = false;
    }

    public boolean getEatable() {
        return eatable;
    }

    public int getNumberOfFruits() {
        return numberOfFruits;
    }

    /**
     * This function decrement the numberOfFruits by one.
     */
    public void eatFruit() {
        this.numberOfFruits -= 1;
    }
}
