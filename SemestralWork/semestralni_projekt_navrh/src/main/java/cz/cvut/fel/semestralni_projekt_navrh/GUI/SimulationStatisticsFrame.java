package cz.cvut.fel.semestralni_projekt_navrh.GUI;

import javax.swing.*;
import java.awt.*;

public class SimulationStatisticsFrame extends JFrame {
    JPanel statisticsMainPanel;

    JLabel treesStartCount;
    JLabel grassStartCount;
    JLabel nightShadeStartCount;
    JLabel rabbitsStartCount;
    JLabel wolvesStartCount;
    JLabel progressLabel;

    JLabel treesCurrentCount;
    JLabel grassCurrentCount;
    JLabel nightShadeCurrentCount;
    JLabel rabbitsCurrentCount;
    JLabel wolvesCurrentCount;
    JLabel averageSatietyCurrentCount;
    JLabel averageSatietyStartCount;
    JLabel rabbitsKilledByWolvesCurrentCount;
    JLabel rabbitsKilledByWolvesAtStart;
    JLabel rabbitsdiedNaturallyCurrentCount;
    JLabel rabbitsDiedNaturallyAtStart;

    public SimulationStatisticsFrame(int[] statistics) {
        // --- MAIN PANELS INITIALIZATION ---
        this.statisticsMainPanel = new JPanel();
        this.statisticsMainPanel.setBounds(0,0,400,50);
        this.statisticsMainPanel.setBackground(new Color(65, 117, 5));
        JPanel statisticsPanel = new JPanel();
        statisticsPanel.setBounds(0, 50, 400, 350);
        statisticsPanel.setLayout(null);
        statisticsPanel.setBackground(new Color(138, 87, 42));
        int xWidthCurrent = 220;
        int xWidthAtStart = xWidthCurrent + 100;

        // --- HEADING PANEL INITIALIZATION ---
        Font fontHeading = new Font("Courier", Font.BOLD,30);
        JLabel heading = new JLabel("Statistics");
        heading.setVerticalAlignment(JLabel.TOP);
        heading.setHorizontalAlignment(JLabel.LEFT);
        heading.setBounds(20, 20, 200, 200);
        heading.setFont(fontHeading);

        Font fontNormal = new Font("Courier", Font.PLAIN,12);
        Font fontBolt = new Font("Courier", Font.BOLD,12);

        // --- INFORMATIONAL LABELS ---
        JLabel currently = new JLabel("Currently:");
        currently.setBounds(xWidthCurrent, 5, 100, 20);
        currently.setFont(fontBolt);
        JLabel atStart = new JLabel("At start:");
        atStart.setBounds(xWidthAtStart, 5, 100, 20);
        atStart.setFont(fontBolt);

        JLabel trees = new JLabel("Trees count:");
        trees.setBounds(10, 30, 150, 30);
        trees.setFont(fontNormal);
        JLabel grass = new JLabel("Grass count:");
        grass.setBounds(10, 60, 150, 30);
        grass.setFont(fontNormal);
        JLabel nightShades = new JLabel("Night Shades count:");
        nightShades.setBounds(10, 90, 150, 30);
        nightShades.setFont(fontNormal);
        JLabel rabbits = new JLabel("Rabbits count:");
        rabbits.setBounds(10, 120, 150, 30);
        rabbits.setFont(fontNormal);
        JLabel wolves = new JLabel("Wolves count:");
        wolves.setBounds(10, 150, 150, 30);
        wolves.setFont(fontNormal);
        JLabel averageSatiety = new JLabel("Average Satiety:");
        averageSatiety.setBounds(10, 180, 150, 30);
        averageSatiety.setFont(fontNormal);
        JLabel rabbitsKilled = new JLabel("Rabbits killed by wolfs:");
        rabbitsKilled.setBounds(10, 210, 180, 30);
        rabbitsKilled.setFont(fontNormal);
        JLabel rabbitsDiedNaturally = new JLabel("Rabbits that died of hunger:");
        rabbitsDiedNaturally.setBounds(10, 240, 210, 30);
        rabbitsDiedNaturally.setFont(fontNormal);

        // --- PROGRESS LABEL ---
        Font fontProgress = new Font("Courier", Font.BOLD,20);
        this.progressLabel = new JLabel("Simulation is running ...");
        this.progressLabel.setBounds(50, 280, 300, 30);
        this.progressLabel.setFont(fontProgress);
        this.progressLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.progressLabel.setVerticalAlignment(SwingConstants.CENTER);

        // --- DATA LABELS ---
        this.treesCurrentCount = new JLabel(statistics[0]+"");
        treesCurrentCount.setBounds(xWidthCurrent, 30, 60, 30);
        treesCurrentCount.setFont(fontNormal);
        this.treesStartCount = new JLabel(statistics[0]+"");
        treesStartCount.setBounds(xWidthAtStart, 30, 60, 30);
        treesStartCount.setFont(fontNormal);

        this.grassCurrentCount = new JLabel(statistics[1]+"");
        grassCurrentCount.setBounds(xWidthCurrent, 60, 60, 30);
        grassCurrentCount.setFont(fontNormal);
        this.grassStartCount = new JLabel(statistics[1]+"");
        grassStartCount.setBounds(xWidthAtStart, 60, 60, 30);
        grassStartCount.setFont(fontNormal);

        nightShadeCurrentCount = new JLabel(statistics[2]+"");
        nightShadeCurrentCount.setBounds(xWidthCurrent, 90, 60, 30);
        nightShadeCurrentCount.setFont(fontNormal);
        this.nightShadeStartCount = new JLabel(statistics[2]+"");
        nightShadeStartCount.setBounds(xWidthAtStart, 90, 60, 30);
        nightShadeStartCount.setFont(fontNormal);

        this.rabbitsCurrentCount = new JLabel(statistics[3]+"");
        rabbitsCurrentCount.setBounds(xWidthCurrent, 120, 60, 30);
        rabbitsCurrentCount.setFont(fontNormal);
        this.rabbitsStartCount = new JLabel(statistics[3]+"");
        rabbitsStartCount.setBounds(xWidthAtStart, 120, 60, 30);
        rabbitsStartCount.setFont(fontNormal);

        this.wolvesCurrentCount = new JLabel(statistics[4]+"");
        wolvesCurrentCount.setBounds(xWidthCurrent, 150, 60, 30);
        wolvesCurrentCount.setFont(fontNormal);
        this.wolvesStartCount = new JLabel(statistics[4]+"");
        wolvesStartCount.setBounds(xWidthAtStart, 150, 60, 30);
        wolvesStartCount.setFont(fontNormal);

        this.averageSatietyCurrentCount = new JLabel(statistics[5]+"");
        averageSatietyCurrentCount.setBounds(xWidthCurrent, 180, 60, 30);
        averageSatietyCurrentCount.setFont(fontNormal);
        this.averageSatietyStartCount = new JLabel(statistics[5]+"");
        this.averageSatietyStartCount.setBounds(xWidthAtStart, 180, 60, 30);
        this.averageSatietyStartCount.setFont(fontNormal);

        this.rabbitsKilledByWolvesCurrentCount = new JLabel(statistics[6]+"");
        this.rabbitsKilledByWolvesCurrentCount.setBounds(xWidthCurrent, 210, 60, 30);
        this.rabbitsKilledByWolvesCurrentCount.setFont(fontNormal);
        this.rabbitsKilledByWolvesAtStart = new JLabel(statistics[6]+"");
        this.rabbitsKilledByWolvesAtStart.setBounds(xWidthAtStart, 210, 60, 30);
        this.rabbitsKilledByWolvesAtStart.setFont(fontNormal);

        this.rabbitsdiedNaturallyCurrentCount = new JLabel(statistics[7]+"");
        this.rabbitsdiedNaturallyCurrentCount.setBounds(xWidthCurrent, 240, 60, 30);
        this.rabbitsdiedNaturallyCurrentCount.setFont(fontNormal);
        this.rabbitsDiedNaturallyAtStart = new JLabel(statistics[7]+"");
        this.rabbitsDiedNaturallyAtStart.setBounds(xWidthAtStart, 240, 60, 30);
        this.rabbitsDiedNaturallyAtStart.setFont(fontNormal);
        // --- VALUE LABELS ---


        // --- ADDING ---
        statisticsPanel.add(currently);
        statisticsPanel.add(atStart);
        statisticsPanel.add(progressLabel);

        statisticsPanel.add(grass);
        statisticsPanel.add(trees);
        statisticsPanel.add(nightShades);
        statisticsPanel.add(rabbits);
        statisticsPanel.add(wolves);
        statisticsPanel.add(averageSatiety);
        statisticsPanel.add(rabbitsKilled);
        statisticsPanel.add(rabbitsDiedNaturally);

        statisticsPanel.add(treesCurrentCount);
        statisticsPanel.add(treesStartCount);
        statisticsPanel.add(grassCurrentCount);
        statisticsPanel.add(grassStartCount);
        statisticsPanel.add(nightShadeCurrentCount);
        statisticsPanel.add(nightShadeStartCount);
        statisticsPanel.add(rabbitsCurrentCount);
        statisticsPanel.add(rabbitsStartCount);
        statisticsPanel.add(wolvesCurrentCount);
        statisticsPanel.add(wolvesStartCount);
        statisticsPanel.add(averageSatietyCurrentCount);
        statisticsPanel.add(averageSatietyStartCount);
        statisticsPanel.add(rabbitsKilledByWolvesCurrentCount);
        statisticsPanel.add(rabbitsKilledByWolvesAtStart);
        statisticsPanel.add(rabbitsdiedNaturallyCurrentCount);
        statisticsPanel.add(rabbitsDiedNaturallyAtStart);

        statisticsMainPanel.add(heading);

        this.add(statisticsPanel);
        this.add(statisticsMainPanel);
        this.setLayout(null);
        this.setSize(400, 400);
        this.add(statisticsPanel);
        this.setVisible(true);
    }

    public void updateStatistics(int[] currentStatistics, boolean simulationEnded) {
        //int numberOfTrees, int numberOfGrass, int numberOfNightShades, int numberOfRabbits, int numberOfWolves, int averageSatietyCount, int rabbitsKilledByWolves, int rabbitsDiedFromHunger, boolean simulationEnded
        this.treesCurrentCount.setText(currentStatistics[0]+"");
        this.grassCurrentCount.setText(currentStatistics[1]+"");
        this.nightShadeCurrentCount.setText(currentStatistics[2]+"");
        this.rabbitsCurrentCount.setText(currentStatistics[3]+"");
        this.wolvesCurrentCount.setText(currentStatistics[4]+"");
        this.rabbitsKilledByWolvesCurrentCount.setText(currentStatistics[6]+"");
        this.averageSatietyCurrentCount.setText(currentStatistics[5]+"");
        this.rabbitsdiedNaturallyCurrentCount.setText(currentStatistics[7]+"");
        if (simulationEnded) {
            this.progressLabel.setText("Simulation has ended!");
        }

        SwingUtilities.updateComponentTreeUI(this);
    }
}
