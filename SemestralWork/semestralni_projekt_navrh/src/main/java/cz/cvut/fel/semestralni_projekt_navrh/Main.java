/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh;

import cz.cvut.fel.semestralni_projekt_navrh.GUI.SimulationMainFrame;
import cz.cvut.fel.semestralni_projekt_navrh.GUI.SimulationStatisticsFrame;

import java.util.concurrent.TimeUnit;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martik
 */
public class Main {
    /**
     * This variable represents logger with following hierarchy: SEVERE - WARNING - INFO - FINE
     */
    public static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws Exception {
        LOG.setLevel(Level.ALL);
        LOG.setUseParentHandlers(false);
        LOG.addHandler(new ConsoleHandler());
        LOG.getHandlers()[0].setLevel(Level.WARNING);
        LOG.config("Application started");


        // --- SETTING SIMULATION VALUES ---
        int size = 10;
        int numberOfGrass = 60;
        int numberOfTrees = 10;
        int numberOfNightShades = 10;
        int numberOfRabbits = 10;
        int numberOfWolves = 2;
        int maxFruits = 5; // Applies only for grass and trees
        int maxFruitsNightShades = 2; // Must be at least 2
        int wolvesInitialSatiety = 10;
        int rabbitsInitialSatiety = 10;
        int speedOfGettingHungryRabbit = 0;
        int speedOfGettingHungryWolf = 0;
        double simulationSpeed = 0.7;
        boolean displaySatietyAndFruits = true;
        int refreshingIntervalInMilliseconds = 30;

        // --- INITIALIZATION OF SIMULATION FIELD ---
        SimulationField simulationField;
        if (numberOfTrees +  numberOfNightShades + numberOfRabbits + numberOfWolves <= size*size && numberOfGrass + numberOfTrees + numberOfNightShades <= size*size) {
            simulationField = new SimulationField(size, simulationSpeed);
        } else {
            LOG.severe("Fatal error: Number of entities doesn't fit on the simulation field");
            throw new RuntimeException("Number of entities doesn't fit on the simulation field");
        }
        // --- INITIALIZATION OF LIST OF ANIMALS AND LIST OF PLANTE ---
        simulationField.createAnimals(numberOfWolves, numberOfRabbits, wolvesInitialSatiety, rabbitsInitialSatiety, speedOfGettingHungryWolf, speedOfGettingHungryRabbit);
        simulationField.createPlants(numberOfTrees, numberOfGrass, numberOfNightShades, maxFruits, maxFruitsNightShades);
        simulationField.initializeSFContainers();


        // --- INITIALIZATION OF GUI ---
        int[] currentStatistics = simulationField.generateStatistics();
        SimulationMainFrame mainFrame = new SimulationMainFrame(simulationField.getSizeX(), simulationField.getSizeY(), simulationField.exportDataForGUI(), displaySatietyAndFruits);
        SimulationStatisticsFrame statisticsFrame = new SimulationStatisticsFrame(currentStatistics);
        TimeUnit.MILLISECONDS.sleep(500);
        simulationField.startSimulationThreads();
        // --- REFRESHING GUI IN INTERVALS ---
        while(true) {
            if(simulationField.areAllAnimalsAreDead()) {
                currentStatistics = simulationField.generateStatistics();
                mainFrame.updateField(simulationField.exportDataForGUI());
                statisticsFrame.updateStatistics(currentStatistics, true);
                break;
            }
            try {
                TimeUnit.MILLISECONDS.sleep(refreshingIntervalInMilliseconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentStatistics = simulationField.generateStatistics();
            mainFrame.updateField(simulationField.exportDataForGUI());
            statisticsFrame.updateStatistics(currentStatistics, false);
        }

        LOG.info("SIMULATION HAS ENDED!");
        System.out.println("SIMULATION HAS ENDED!");
    }
}
