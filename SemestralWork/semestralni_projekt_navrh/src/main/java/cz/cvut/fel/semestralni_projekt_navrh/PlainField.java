package cz.cvut.fel.semestralni_projekt_navrh;

/**
 * This class represents a plain field, where the entities can be put.
 */
public class PlainField extends Entity {

    public PlainField(int[] coordinates) {
        super(coordinates);
        this.imagePath = "semestralni_projekt_navrh/images/nothing.png";
        this.viewImportance = 3;
        this.type = EntityTypes.PLAIN_FIELD;
        this.canBeSteppedOn = true;
        this.isFood = false;
    }
}
