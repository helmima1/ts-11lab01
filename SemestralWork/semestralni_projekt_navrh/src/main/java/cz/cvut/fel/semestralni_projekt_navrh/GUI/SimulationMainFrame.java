package cz.cvut.fel.semestralni_projekt_navrh.GUI;

import cz.cvut.fel.semestralni_projekt_navrh.GUIDataWrapper;

import javax.swing.*;
import java.awt.*;

public class SimulationMainFrame extends JFrame {
    JLabel[][] playingField;
    JPanel simulationPanel;
    int sizeX;
    int sizeY;
    boolean displaySatietyAndFruit;

    public SimulationMainFrame(int sizeX, int sizeY, GUIDataWrapper[][] stringSimulationField, boolean displaySatietyAndFruit) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.displaySatietyAndFruit = displaySatietyAndFruit;
        // Building panels
        this.simulationPanel = new JPanel();
        this.simulationPanel.setBounds(0,0,sizeX*40,sizeY*40);
        this.simulationPanel.setLayout(new GridLayout(sizeY,sizeX, 0, 0));
        // Building grid
        this.playingField = new JLabel[sizeX][sizeY];

        // Initializing labels with addresses
        for(int i = 0; i < this.sizeX; i++) {
            for(int j = 0; j < this.sizeY; j++) {
                ImageIcon picture = new ImageIcon(stringSimulationField[i][j].imageAddress);

                // -----
                if (displaySatietyAndFruit) {
                    Font font = new Font("Courier", Font.BOLD,15);
                    if (stringSimulationField[i][j].hasFruits && stringSimulationField[i][j].hasSatiety) {
                        this.playingField[i][j] = new JLabel(stringSimulationField[i][j].getNumberOfFruits() + " " + stringSimulationField[i][j].getSatiety());
                        this.playingField[i][j].setForeground(Color.blue);
                    } else if (stringSimulationField[i][j].hasFruits) {
                        this.playingField[i][j] = new JLabel(stringSimulationField[i][j].getNumberOfFruits() + "");
                        this.playingField[i][j].setForeground(Color.BLACK);
                    } else if (stringSimulationField[i][j].hasSatiety) {
                        this.playingField[i][j] = new JLabel(stringSimulationField[i][j].getSatiety() + "");
                        this.playingField[i][j].setForeground(Color.blue);
                    } else {
                        this.playingField[i][j] = new JLabel();
                    }
                    this.playingField[i][j].setFont(font);
                    playingField[i][j].setHorizontalTextPosition(JLabel.CENTER);
                } else {
                    this.playingField[i][j] = new JLabel();
                }
                // --------

                this.playingField[i][j].setIcon(picture);
                this.playingField[i][j].setBounds(i*40, j*40, 40,40);
                this.simulationPanel.add(this.playingField[i][j]);
            }
        }

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setSize(this.sizeX*40, this.sizeY*40+30);
        //this.add(panel);
        //this.add(playingField[4][3]);
        this.add(this.simulationPanel);
        //this.add(playingField[9][9]);
        this.setVisible(true);
    }

    /**
     * This function updates ImageIcon adresses.
     *
     * @param dataInField
     */
    public void updateField(GUIDataWrapper[][] dataInField) {
        //long time = currentTimeMillis();
        //this.simulationPanel.removeAll();
        for(int i = 0; i < this.sizeX; i++) {
            for(int j = 0; j < this.sizeY; j++) {
                //System.out.println("i:"+i+" j:"+j+" sizeX:"+sizeX+" sizeY:"+sizeY);
                ImageIcon picture = new ImageIcon(dataInField[i][j].imageAddress);
                //ImageIcon picture = new ImageIcon(dataInField[i][j].imageAddress);

                //----
                if (this.displaySatietyAndFruit) {
                    Font font = new Font("Courier", Font.BOLD,15);
                    if (dataInField[i][j].hasFruits && dataInField[i][j].hasSatiety) {
                        this.playingField[i][j].setText(dataInField[i][j].getNumberOfFruits() + " " + dataInField[i][j].getSatiety());
                        this.playingField[i][j].setForeground(Color.black);
                    } else if (dataInField[i][j].hasFruits) {
                        this.playingField[i][j].setText(dataInField[i][j].getNumberOfFruits() + "");
                        this.playingField[i][j].setForeground(Color.BLACK);
                    } else if (dataInField[i][j].hasSatiety) {
                        this.playingField[i][j].setText(dataInField[i][j].getSatiety() + "");
                        this.playingField[i][j].setForeground(Color.blue);
                    } else {
                        this.playingField[i][j].setText("");
                    }
                    this.playingField[i][j].setFont(font);
                } else {
                    this.playingField[i][j].setText("");
                }
                // -----

                //this.playingField[i][j] = new JLabel();
                //playingField[i][j].setHorizontalTextPosition(JLabel.CENTER);
                this.playingField[i][j].setIcon(picture);
                this.simulationPanel.add(this.playingField[i][j]);
            }
        }
        SwingUtilities.updateComponentTreeUI(this);
        //time = currentTimeMillis() - time;
        //System.out.println("Time to update gui: "+time);
    }
}
