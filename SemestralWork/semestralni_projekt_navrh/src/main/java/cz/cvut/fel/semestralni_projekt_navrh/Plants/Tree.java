/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Plants;

/**
 *
 * @author martik
 */
public class Tree extends Plant {
    // Tree also has numberOfFruits but initialized to zero.

    public Tree(int[] coordinates) {
        super(0, coordinates);
        this.canBeLayered = false;
        this.imagePath = "semestralni_projekt_navrh/images/pine-tree.png";
        this.type = EntityTypes.TREE;
        this.isFood = false;
    }
}
