/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Animals;

import cz.cvut.fel.semestralni_projekt_navrh.Entity;

import java.util.Arrays;

import static cz.cvut.fel.semestralni_projekt_navrh.Main.LOG;
import static java.lang.System.currentTimeMillis;

/**
 * Rabbit extends class animal and posesses folowing abilities: ---
 *
 * @author martik
 */
public class Rabbit extends Animal implements Runnable {
    Animal[] pointerToItsList;
    public boolean killedByWolf;
    public boolean killedByNightShade;

    public Rabbit(int[] coordinates, Animal[] pointerToItList, int animalID, double simulationSpeed, int satiety, int speedOfGettingHungry) {
        super(coordinates, animalID, simulationSpeed, satiety, speedOfGettingHungry);
        this.killedByWolf = false;
        this.killedByNightShade = false;
        this.imagePath = "semestralni_projekt_navrh/images/rabbit-brown.png";
        this.viewImportance = 1;
        this.type = EntityTypes.RABBIT;
        this.edibleFood = new EntityTypes[] {EntityTypes.GRASS, EntityTypes.NIGHT_SHADE};
        this.pointerToItsList = pointerToItList;
        this.isFood = true;
    }

    public void setPointerToItsList(Animal[] animals) {
        this.pointerToItsList = animals;
    }

    /**
     * Executes full sequence of steps to finish eating. This includes increasing satiety, killing plants if needed or killing animal if food is poisenous.
     *
     * @param fieldToEat
     * @returns true, if it beaks
     */
    public boolean eat(Entity fieldToEat) {
        if (fieldToEat != null) {
            LOG.fine("Rabbit with id "+animalID+" has found chosen to eat food of type "+fieldToEat.type+" on coordinates "+Arrays.toString(fieldToEat.getCoordinates()));
            // --- EAT PLANT ---
            Entity.convertEntityToPlantWithFruits(fieldToEat).eatFruit();
            this.satiety += 1;
            if (Entity.convertEntityToPlantWithFruits(fieldToEat).getNumberOfFruits() == 0) {
                fieldToEat.die();
                LOG.fine("Entity of type "+fieldToEat.type+" on coordinates "+Arrays.toString(fieldToEat.getCoordinates())+" has no more frits, therefore it dies");
            }
            // --- DIE IF POISONOUS NIGHT SHADE WAS EATEN ---
            if(fieldToEat.type == EntityTypes.NIGHT_SHADE) {
                this.die();
                this.killedByNightShade = true;
                LOG.fine("Rabbit with id "+animalID+" was killed by poisenous Níight shade!");
                return true;
                //break;
            }
        } else {
            LOG.fine("No food was found for Rabbit with id "+animalID);
        }
        return false;
    }

    public void move(int[] fieldToMove) {
        if (fieldToMove != null) {
            LOG.fine("Rabbit with id "+animalID+" has determined to move to "+Arrays.toString(fieldToMove));
            //System.out.println("Králík with id " + animalID + " is moving to " + Arrays.toString(fieldToMove));
            this.pointerToSF[this.getXCoordinates()][this.getYCoordinates()].removeEntity(this);
            this.pointerToSF[fieldToMove[0]][fieldToMove[1]].updateListValue(this);
            this.pointerToItsList[animalID].setCoordinates(fieldToMove);
        } else {
            LOG.fine("There is no way for Rabbit with id"+animalID+" to move.");
        }
    }

    @Override
    public void run() {
        LOG.info("Rabbit with id "+animalID+" is running!");
        int cyclesCounter = 1;
        while(this.active) {
            // --- LOCKING THREAD FOR FINDING FOOD AND EATING ---
            LOG.fine("Rabbit with id "+animalID+" is currently on coordinates "+Arrays.toString(this.getCoordinates()));
            this.lock.lock();
            try {
                // --- SCOUT FOR FOOD FIELDS AND CHOOSE ONE ---
                Entity[] fieldsToEat = this.scoutForFood();
                Entity fieldToEat = this.findRandomFood(fieldsToEat);
                // if fieldToEat is null than it didn't find any food
                boolean breaks = this.eat(fieldToEat);
                if(breaks) {
                    break;
                }
            } finally {
                this.lock.unlock();
            }
            // --- LOCKING THREAD FOR FINDING PLACE TO MOVE AND MOVING ---
            this.lock.lock();
            try {
                //System.out.println("Králík with id " + animalID + " is on coordinates " + Arrays.toString(this.coordinates));
                // --- SCOUT FOR PLACES TO MOVE ---
                int[][] fieldsToMove = this.scoutForPlacesToMove();
                // --- MOVE TO THAT POSITION ---
                int[] fieldToMove = this.findRandomPlaceToMove(fieldsToMove);
                this.move(fieldToMove);
            } finally {
                lock.unlock();
            }

            // --- WAIT ---
            try {
                int origin = (int) ((int) 1000 * (simulationSpeed));
                int bound = (int) ((int) 2000 * (simulationSpeed));
                int timeToWait = Entity.rand.nextInt(origin, bound);
                LOG.fine("Rabbit with id "+animalID+" has done all required moves and is now sleeping for "+timeToWait+" miliseconds.");
                Thread.sleep(timeToWait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // --- CYCLES COUNT FOR TESTING ---
            cyclesCounter += 1;
            // --- DECREASE SATIETY EVERY 3 ROUNDS ---
            if (cyclesCounter % 3 == 0) {
                this.satiety -= 1;
            }
            // --- DIE OF HUNGER (SATIETY == 0) ---
            if(this.satiety == 0) {
                LOG.info("Rabbit with id "+animalID+" has died from hunger");
                this.die();
            }
        }
        LOG.info("Rabbit with id "+animalID+" has ended his thread!");
    }
}
