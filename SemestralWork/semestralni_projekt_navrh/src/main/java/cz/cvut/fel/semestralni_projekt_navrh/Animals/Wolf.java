/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cz.cvut.fel.semestralni_projekt_navrh.Animals;

import cz.cvut.fel.semestralni_projekt_navrh.Entity;

import java.util.Arrays;

import static cz.cvut.fel.semestralni_projekt_navrh.Main.LOG;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.nanoTime;

/**
 *
 * @author martik
 */
public class Wolf extends Animal implements Runnable {
    Animal[] poinerToItsList;

    public Wolf(int[] coordinates, Animal[] pointerToItList, int animalID, double simulationSpeed, int satiety, int speedOfGettingHungry) {
        super(coordinates, animalID, simulationSpeed, satiety, speedOfGettingHungry);
        this.imagePath = "semestralni_projekt_navrh/images/wolf-brown.png";
        this.viewImportance = 1;
        this.type = EntityTypes.WOLF;
        this.edibleFood = new EntityTypes[] {EntityTypes.RABBIT};
        this.poinerToItsList = pointerToItList;
        this.isFood = false;
    }

    @Override
    public void run() {
        long generalTime = currentTimeMillis();
        LOG.info("Wolf with id " + animalID + " is running!");
        int cyclesCounter = 0;
        while (this.active) {
            LOG.fine("Wolf with id "+animalID+" is currently on coordinates "+Arrays.toString(this.getCoordinates()));
            // --- LOCKING THREAD FOR FINDING FOOD AND EATING ---
            this.lock.lock();
            try {
                // --- SCOUT FOR FOOD FIELDS AND CHOOSE ONE ---
                Entity[] fieldsToEat = this.scoutForFood();
                Entity fieldToEat = this.findRandomFood(fieldsToEat);
                // if fieldToEat is null than it didn't find any.
                if (fieldToEat != null) {
                    LOG.fine("Rabbit with id "+animalID+" has found chosen to eat food of type "+fieldToEat.type+" on coordinates "+Arrays.toString(fieldToEat.getCoordinates()));
                    // --- EAT RABBIT (he dies) ---
                    this.satiety += Entity.convertEntityToRabbit(fieldToEat).satiety; // Wolf gets as much satiety as he has
                    Entity.convertEntityToRabbit(fieldToEat).die();
                    Entity.convertEntityToRabbit(fieldToEat).killedByWolf = true;
                } else {
                    LOG.fine("No food was found for Wolf with id "+animalID);
                }
            } finally {
                this.lock.unlock();
            }
            // --- LOCKING THREAD FOR FINDING PLACE TO MOVE AND MOVING ---
            this.lock.lock();
            try {
                // --- SCOUT FOR PLACES TO MOVE ---
                int[][] fieldsToMove = this.scoutForPlacesToMove();
                // --- MOVE TO THAT POSITION ---
                int[] fieldToMove = this.findRandomPlaceToMove(fieldsToMove);
                if (fieldToMove != null) {
                    System.out.println("Wolf with id " + animalID + " is moving to " + Arrays.toString(fieldToMove));
                    this.pointerToSF[this.getXCoordinates()][this.getYCoordinates()].removeEntity(this);
                    this.pointerToSF[fieldToMove[0]][fieldToMove[1]].updateListValue(this);
                    this.poinerToItsList[animalID].setCoordinates(fieldToMove);
                    Thread.sleep(10);
                } else {
                    System.out.println("There is no way for Wolf with id " + animalID + " to move.");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }

            // --- WAIT ---
            try {
                int origin = (int) ((int) 1000 * (simulationSpeed));
                int bound = (int) ((int) 2000 * (simulationSpeed));
                int timeToWait = Entity.rand.nextInt(origin, bound);
                LOG.fine("Wolf with id "+animalID+" has done all required moves and is now sleeping for "+timeToWait+" miliseconds.");
                Thread.sleep(timeToWait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // --- CYCLES COUNT FOR TESTING ---
            cyclesCounter += 1;
            // --- DECREASE SATIETY EVERY 3 ROUNDS ---
            if (cyclesCounter % 5 == 0) {
                satiety -= 1;
            }
            // --- DIE OF HUNGER (SATIETY == 0) ---
            if(satiety == 0) {
                this.die();
            }
        }
        System.out.println("Wolf with id "+animalID+" has ended his thread!");
    }
}
