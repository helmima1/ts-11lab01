import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SpringerTest {

    private WebDriver driver;

    private LogInPage loginPage;
    private AdvancedSearch advancedSearch;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test
    public void runSelenium() throws InterruptedException {
        // Login user (my own account)
        loginPage = new LogInPage(driver);
        loginPage.login("mhel@email.cz", "N0veHeslo123");
        loginPage.pressLogin();

        // Advanced search
        advancedSearch = new AdvancedSearch(driver);
        advancedSearch.fillSearch("Page Object Model", "Sellenium Testing", "2022");
        advancedSearch.submitAdvancedSearch();

        // Get data from articles
        advancedSearch.getArticles(4);
    }
}
