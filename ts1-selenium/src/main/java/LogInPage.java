import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage {
    private final WebDriver driver;

    @FindBy(id = "login-box-email")
    private WebElement emailInput;
    @FindBy(id = "login-box-pw")
    private WebElement passwordInput;


    public LogInPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.get("https://link.springer.com/signup-login");
        //System.out.println("emailInput is: "+emailInput);
    }

    public void login(String email, String password) {
        emailInput.sendKeys(email);
        passwordInput.sendKeys(password);
        //loginButton.click();
    }
    
    public void pressLogin() {
        this.driver.findElement(By.xpath("//div[@class='form-submit']/button")).click();
    }
}
