import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class AdvancedSearch {
    private final WebDriver driver;

    @FindBy(id = "all-words")
    private WebElement allWords;
    @FindBy(id = "least-words")
    private WebElement leastWords;
    @FindBy(id = "submit-advanced-search")
    private WebElement submitAdvancedSearch;
    @FindBy(id = "facet-start-year")
    private WebElement yearStart;
    @FindBy(id = "facet-end-year")
    private WebElement yearEnd;
    @FindBy(tagName = "li")
    private WebElement listOFArticles;

    public AdvancedSearch(WebDriver driver) {
        this.driver = driver;
        driver.get("https://link.springer.com/advanced-search");
        PageFactory.initElements(driver, this);
    }

    public void fillSearch(String allWords, String leastWords, String year) {
        this.allWords.sendKeys(allWords);
        this.leastWords.sendKeys(leastWords);
        this.yearStart.sendKeys(year);
        this.yearEnd.sendKeys(year);
    }

    public void submitAdvancedSearch() throws InterruptedException {
        //Thread.sleep(5000);
        WebElement label = this.driver.findElement(By.xpath("//div[@class='form-submit-section']/button"));
        submitAdvancedSearch.submit();
        //label.click();
    }

    public void getArticles(int count) {
        System.out.println("HAHAH");
        List<WebElement> listOfArticlesString = this.listOFArticles.findElements(By.tagName("li"));
        for (WebElement webElement : listOfArticlesString) {
            System.out.println(webElement.findElement(By.tagName("a")));
        }


    }

}
