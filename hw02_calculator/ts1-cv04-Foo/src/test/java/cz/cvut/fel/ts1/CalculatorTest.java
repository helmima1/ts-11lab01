package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

public class CalculatorTest {

    private static Stream<Arguments> providerMethod() {
        return Stream.of(
                Arguments.of(1,2,3),
                Arguments.of(1,2,3)
        );
    }

    static Calculator f;
    @BeforeAll
    public static void initVariable() {
        f = new Calculator();
    }

    @ParameterizedTest
    @MethodSource()
    public void additionTest(int a, int b, int c) {
        // ARRANGE
        Calculator c = new Calculator(); // Vytvářím instanci, to Calculator na začátku specifikuje třídu!
        //act
        int res = c.add(2,3);
        // assert
        Assertions.assertEquals(5, res);
    }

    @ParameterizedTest
    @ValueSource(ints = {2, -4, 8, 10})
    public void add3and3_5() {
        // ARRANGE
        Calculator c = new Calculator(); // Vytvářím instanci, to Calculator na začátku specifikuje třídu!
        //act
        int res = c.add(2, 3);
        // assert
        assertTrue(res);
    }

    @Test
    public void add3and3_5() {
        // ARRANGE
        Calculator c = new Calculator(); // Vytvářím instanci, to Calculator na začátku specifikuje třídu!
        //act
        int res = c.add(2,3);
        // assert
        Assertions.assertEquals(5, res);
    }

    @Test
    public void divide_by_zero() {
        // ARRANGE
        Calculator c = new Calculator();
        Exception exception = Assertions.assertThrows(Exception.class, () -> f.exceptionThrown());

 //       Exception exception = Assertions.assertThrows(Exception.class, new Executable() {
//            @Override
//            public void execute() throws Throwable {
//                c.exeptionThrown();
//            }
//        });

        String expectedMessage = "Ocekavana vyjimka";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);

    }

    @Test
    public void multiply3by3_9() {
        Calculator c = new Calculator();

        Assertions.assertEquals(9, c.multiply(3, 3));
    }

    @Test
    public void subtract3by3_0() {
        Calculator c = new Calculator();

        Assertions.assertEquals(0, c.subtract(3, 3));
    }
}
